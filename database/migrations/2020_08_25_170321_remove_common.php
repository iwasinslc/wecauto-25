<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCommon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_pieces', function (Blueprint $table) {
            $table->dropColumn('common');
        });

        Schema::table('exchange_orders', function (Blueprint $table) {
            $table->dropColumn('common');
        });

        Schema::table('order_requests', function (Blueprint $table) {
            $table->dropColumn('common');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
