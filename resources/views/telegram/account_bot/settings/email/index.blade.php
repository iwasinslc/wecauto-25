<?php
$user = $telegramUser->user()->first();

if (null == $user) {
    die('ok');
}
?>
@if(!empty($user->email) && $user->email != $telegramUser->telegram_user_id)
✉️ <b>E-mail:</b> {{ $user->email }}{{ $user->isVerifiedEmail() ? ' ('.__('подтвержден').')' : ' ('.__('не подтвержден').')' }}

@else
{{__('E-mail not installed')}}.
@endif