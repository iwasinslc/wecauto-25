@extends('layouts.profile')
@section('title', str_replace(' ', '', __('Convert')))
@section('content')

    <section>
        <div class="container">
            <h3 class="lk-title">{{__('Convert')}}
            </h3>
            <!-- Start convert form-->
            <div class="convert-module white-block">
                <form method="POST" action="{{ route('profile.convert.store') }}">
                    {{ csrf_field() }}
                    <div class="convert-module__row">
                        <div class="convert-module__col">
                            <!-- .field--error-->
                            <div class="field">
                                <label>{{__('Amount')}}, <span class="color-gray" id="currency">ACC</span></label>
                                <div class="field-group">
                                    <input type="number" step="any" id="amount" value="0" name="amount">
                                    <div class="field-group__subtext">
                                        <label class="select">
                                            <select id="wallet_id" name="wallet_id" class="" autofocus>
                                                <option data-rate="{{rate('ACC', 'USD')*rate('USD', 'FST')}}" data-currency="ACC" value="{{getUserWallet('ACC')['id']}}">ACC</option>
                                                <option data-rate="{{rate('GNT', 'USD')*rate('USD', 'FST')}}" data-currency="GNT"  value="{{getUserWallet('GNT')['id']}}">GNT</option>

                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="convert-module__col">
                            <div class="field">
                                <label>{{__('Amount')}}, <span class="color-gray">FST</span></label>
                                <div class="field-group field-group--disabled">
                                    <input type="number" id="fst" step="any" value="0" name="amount_fst" disabled>
                                    <div class="field-group__subtext">FST
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn--warning">{{__('Convert')}}
                    </button>
                </form>
            </div>
            <!-- End convert form-->
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')
    <script>

        $('body').on('change', '#wallet_id', function () {
            var amount = $('#amount').val();
            var rate = $(this).find(':selected').data('rate');
            var currency = $(this).find(':selected').data('currency');
            $('#fst').val((amount * rate));
            $('#currency').html(currency);
        });

        $('body').on('keyup input', '#amount', function () {
            var amount = $(this).val();
            var rate = $('#wallet_id').find(':selected').data('rate');
            var currency = $('#wallet_id').find(':selected').data('currency');
            $('#fst').val((amount * rate));
            $('#currency').html(currency);
        });
    </script>
@endpush

