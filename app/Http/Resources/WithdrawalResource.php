<?php

namespace App\Http\Resources;

use App\Models\Withdraw;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="WithdrawalResource",
 *     description="Withdrawal resource",
 *     type="object",
 *     @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Withdrawal"))
 * )
 */
class WithdrawalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Withdraw $this */
        return [
            'id' => $this->id,
            'payment_system' => $this->paymentSystem->name,
            'value' => $this->amount,
            'currency' => $this->currency->code,
            'status' => $this->status->name,
            'batch_id' => $this->batch_id,
            'source' => $this->source,
            'result' => $this->result,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
