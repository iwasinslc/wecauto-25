<?php
namespace App\Http\Controllers\Telegram\account_bot\Settings\PersonalData;

use App\Http\Controllers\Controller;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Models\User;
use App\Modules\Messangers\TelegramModule;
use App\Http\Controllers\Telegram\account_bot\Settings\PersonalData\SetupNewEmail\EnterNewEmailController;

class EmailController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index(
        TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event
    ) {
        sleep(1);
        $scope = TelegramBotScopes::where('command', '📧 E-mail')
            ->where('bot_keyword', $bot->keyword)
            ->first();

        TelegramModule::setLanguageLocale($telegramUser->language);

        $user = $telegramUser->user;

        if (null == $user) {
            return response('ok');
        }

//        if ($user->settingsChangesCount('email')==0) {
//            $this->haventChanges($webhook, $bot, $scope, $telegramUser, $event);
//            return response('ok');
//        }

        $message = view('telegram.account_bot.settings.email.index', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        /** @var User $user */
        $user = $telegramUser->user()->first();

        if (null == $user) {
            return response('ok');
        }

        $keyboard = null;

        $changeEmailButton = $user->email != $telegramUser->telegram_user_id
            ? __('Change E-Mail')
            : __('Install E-Mail');

        if (!empty($user->email) && $user->email != $telegramUser->telegram_user_id ) {
            if (false === $user->isVerifiedEmail())
            {
                $keyboard = [
                    [
                        ['text' => __('Send new mail'), 'callback_data' => 'send_new_email_verification_code'],
                    ],
                    [
                        ['text' => $changeEmailButton, 'callback_data' => 'enter_new_email'],
                    ],
                ];
            }
            else
            {
                $keyboard = [
                    [
                        ['text' => $changeEmailButton, 'callback_data' => 'enter_new_email'],
                    ],
                ];
            }

        } else {
            app()->call(EnterNewEmailController::class.'@index', [
                'webhook' => $webhook,
                'bot' => $bot,
                'scope' => $scope,
                'telegramUser' => $telegramUser,
                'event' => $event,
            ]);
            return response('ok');
        }

        $keyboard = [
            'inline_keyboard' => $keyboard,
            'resize_keyboard' => true,
        ];
        
        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage(
                $event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                $keyboard,
                $scope,
                'inline_keyboard'
            );
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }


}
