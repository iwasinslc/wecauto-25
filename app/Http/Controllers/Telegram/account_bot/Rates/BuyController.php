<?php
namespace App\Http\Controllers\Telegram\account_bot\Rates;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\account_bot\StartController;
use App\Models\AutoCreateDeposit;
use App\Models\Deposit;
use App\Models\Rate;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class BuyController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        preg_match('/finish\_deposit ([a-zA-Z0-9-_]+)/', $event->text, $data);

        $user = $telegramUser->user;

        if (isset($data[1]))
        {
            $buy_data = cache()->get('create_deposit'.$user->id);
            if ($buy_data==null)
            {
                $error = __('Time to buy the lot has expired');
                $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
                return response('ok');
            }
            if ($data[1]=='yes')
            {
                $buy_data['user'] = $user;

                if ($buy_data['type_id'] == '2') {
                    Deposit::addDeposit($buy_data);
                    $message = view('telegram.account_bot.rates.dismiss', [
                        'webhook'      => $webhook,
                        'bot'          => $bot,
                        'scope'        => $scope,
                        'telegramUser' => $telegramUser,
                        'event'        => $event,
                    ])->render();
                }
                else{
                    $walletId = $buy_data['wallet_id'];
                    $rateId = $buy_data['rate_id'];
                    $amount = (float) $buy_data['amount'];


                    $rate = Rate::find($rateId);

                    if (null === $rate) {
                        \Log::critical('Wrong rate id');
                    }


                    $wallet = $user->wallets()->where('id', $walletId)->first();

                    if (null === $wallet) {
                        \Log::critical('Wrong wallet id');
                    }

                    $currency = $wallet->currency;

                    $paymentSystem = $wallet->paymentSystem;


                    $psMinimumTopupArray = @json_decode($paymentSystem->minimum_topup, true);
                    $psMinimumTopup      = isset($psMinimumTopupArray[$currency->code])
                        ? $psMinimumTopupArray[$currency->code]
                        : 0;

                    if ($amount < $psMinimumTopup) {
                        $error = __('Minimum balance recharge is').' '.$psMinimumTopup.$currency->symbol;
                        $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
                        return response('ok');
                    }


                    AutoCreateDeposit::create([
                        'wallet_id' => $wallet->id,
                        'rate_id' => $rate->id,
                        'amount' => $amount,
                        'user_id' => $user->id,
                    ]);

                    $message = view('telegram.account_bot.topup.user_answer_success', [
                        'webhook'      => $webhook,
                        'bot'          => $bot,
                        'scope'        => $scope,
                        'telegramUser' => $telegramUser,
                        'link'         => route('telegram.topup.' . $paymentSystem->code, ['id'=>$user->id]),
                    ])->render();

                    cache()->put('topup_data'.$user->id, [
                        'payment_system_id'=>$paymentSystem->id,
                        'currency_id'=>$currency->id,
                        'amount'=>$amount
                    ], 60);
                }




                cache()->forget('create_deposit'.$user->id);



                if (config('app.env') == 'develop') {
                    \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
                }


                try {
                    $telegramInstance = new TelegramModule($bot->keyword);
                    $telegramInstance->sendMessage($event->chat_id,
                        $message,
                        'HTML',
                        true,
                        false,
                        null,
                        null,
                        $scope,
                        'inline_keyboard');
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    return response('ok');
                }
                return response('ok');
            }
            else
            {
                cache()->forget('create_deposit'.$user->id);
                app()->call(StartController::class.'@index', [
                    'webhook' => $webhook,
                    'bot' => $bot,
                    'scope' => $scope,
                    'telegramUser' => $telegramUser,
                    'event' => $event,
                ]);
                return response('ok');
            }
        }
        else{
            return response('ok');
        }

    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function validationFailed(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        string $error
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.rates.error', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'error' => $error
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }
}