<?php
namespace App\Http\Requests;

use App\Rules\RuleCheckRate;
use App\Rules\RuleDepositWalletExist;
use App\Rules\RuleEnoughBalance;
use App\Rules\RuleFSTEnoughBalance;
use App\Rules\RuleFSTRange;
use App\Rules\RulePlanRange;
use App\Rules\RuleRateCurrency;
use App\Rules\RuleWalletExist;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\RuleUUIDEqual;

/**
 * Class RequestCreateDeposit
 * @package App\Http\Requests
 *
 * @property string wallet_id
 * @property string rate_id
 * @property float amount
 */
class RequestAddFst extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_id' => ['required', new RuleDepositWalletExist, new RuleUUIDEqual],
            'deposit_id'   => ['required', 'exists:deposits,id', new RuleUUIDEqual],
            'count'    => ['integer', new RuleFSTRange, new RuleFSTEnoughBalance]
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'wallet_id.required' => __('Wallet is required'),

            'rate_id.required'   => __('Rate is required'),
            'rate_id.exists'     => __('Rate is not exists'),

            'count.numeric'     => __('Amount have to be numeric'),
        ];
    }
}
