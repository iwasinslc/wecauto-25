<?php
namespace App\Rules;

use App\Models\PaymentSystem;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

/**
 * Class RuleWalletExist
 * @package App\Rules
 */
class RuleWithdrawWalletExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        $extractCurrency = explode(':', request()->currency);

        if (count($extractCurrency) != 2) {
            return back()->with('error', __('Unable to read data from request'))->withInput();
        }

        $paymentSystem = PaymentSystem::where('id', $extractCurrency[1])->first();
        $currency = $paymentSystem->currencies()->where('id', $extractCurrency[0])->first();

        $wallet = user()->wallets()->where('currency_id', $currency->id)->where('payment_system_id', $paymentSystem->id)->first();

        if (empty($wallet)) {
            return false;
        }

        return null !== $wallet;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.wrong_wallet');
    }
}
